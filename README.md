# mt-testing
Tests:
  1. SealedSecrets test

kubeseal installation client-side:
```
  mkdir bin
  wget https://github.com/bitnami-labs/sealed-secrets/releases/latest/download/kubeseal-linux-amd64 -O bin/kubeseal
  chmod +x bin/kubeseal
```

-----------------
### 1. SealedSecrets test:

```
  argocd app create ss-test --repo https://gitlab.cern.ch/aduginet/mt-testing.git --path ss-test --dest-server https://kubernetes.default.svc --dest-namespace kube-system
  argocd sync ss-test
```
``
Will deploy two SealedSecrets:
 - `s-test-secret`: secret available in the kube-system namespace (namespace-wide scope)
 - `ss-test-cluster-secret` secret available cluster-wide

To create `ss-test-secret`:
```
  echo -n 'This is a test!' | kubectl create secret generic ss-test-secret --dry-run=client --from-file=name=/dev/stdin -o yaml | ./bin/kubeseal --namespace kube-system --scope namespace-wide --name ss-test-secret > ss-test/ss-test-secret_sealed.yaml
  echo -n 'This secret is only available in the kube-system namespace.' | kubectl create secret generic ss-test-secret --dry-run=client --from-file=comment=/dev/stdin -o yaml | ./bin/kubeseal --namespace kube-system --scope namespace-wide --name ss-test-secret --merge-into ss-test/ss-test-secret_sealed.yaml
```

To create `s-test-cluster-secret`
```
  echo -n 'This is a test!' | kubectl create secret generic ss-test-cluster-secret --dry-run=client --from-file=name=/dev/stdin -o yaml | ./bin/kubeseal --scope cluster-wide --namespace kube-system --name ss-test-cluster-secret > ss-test/ss-test-cluster-secret_sealed.yaml
  echo -n 'This secret is available cluster-wide.' | kubectl create secret generic ss-test-cluster-secret --dry-run=client --from-file=comment=/dev/stdin -o yaml | ./bin/kubeseal --scope cluster-wide --namespace kube-system --name ss-test-cluster-secret --merge-into ss-test/ss-test-cluster-secret_sealed.yaml
```

See [SealedSecrets](https://github.com/bitnami-labs/sealed-secrets).
